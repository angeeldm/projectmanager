import React from 'react';
import Header from './components/Header';
import ProjectsList from './components/ProjectsList/ProjectsList';
import { Button } from "@material-ui/core";
import {NavContainer, NavBox, Title} from './styles/NavBar';
import { useHistory} from 'react-router-dom';

const App = () => {
  const history = useHistory();
  
  return (
    <>
      <Header />
      <NavContainer>
        <NavBox>
          <Title>My Projects</Title>
          <Button variant="contained" color="secondary" onClick={() => history.push('/create-project')}>+ Add Project</Button>
        </NavBox>
      </NavContainer>

      <ProjectsList />
    </>
  );
}
 
export default App;