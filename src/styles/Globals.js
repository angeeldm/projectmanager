import styled from "styled-components";

const Container = styled.div`
    padding: 10px 25px;
    background-color: #FFF;
`;

const Message = styled.p`
    text-align: center;
    font-size: 22px;
    color: #c1c1c1;
    padding: 15px 0;
`;

export {Container, Message}