import styled from "styled-components";

const NavContainer = styled.div`
    border-top: 1px solid #D9D9D9;
    border-bottom: 1px solid #D9D9D9;
    width: 100%;
    background-color: #FFF;
`;

const NavBox = styled.div`
    padding: 8px 0;
    box-sizing: border-box;
    width: 95%;
    margin: 0 auto;
    display: flex;
    align-items: center;
    justify-content: ${props => props.BackBtn ? "" : "space-between"};

    button{
        background-color: #F5222D;
        color: #FFF;
    }

    a{
        color: #000;
        margin-right: 25px;
    }
`;

const Title = styled.h1`
    margin: 0;
    font-size: 24px;

    @media(min-width: 768px){
        font-size: 34px;
    }
`;

export {NavContainer, NavBox, Title}