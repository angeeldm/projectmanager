import React from 'react';
import { useHistory } from 'react-router';
import { Container, Message } from '../styles/Globals';

const Error404 = () => {
    const history = useHistory()
    setTimeout(() => {
        history.push('/')
    }, 1300);
    return (
        <Container>
            <Message>Page not found</Message>
        </Container>
    );
}
 
export default Error404;