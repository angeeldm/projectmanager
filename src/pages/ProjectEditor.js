import React from 'react';
import Header from '../components/Header';
import {NavContainer, NavBox, Title} from '../styles/NavBar';
import ProjectForm from '../components/ProjectForm/ProjectForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Link, useParams } from 'react-router-dom';

const ProjectEditor = () => {
    const {id} = useParams();

    return (
        <>
            <Header />
            <NavContainer>
                <NavBox BackBtn>
                    <Link to="/"> <FontAwesomeIcon icon={faArrowLeft} /> Back</Link>
                    <Title>{id ? 'Edit Project' : 'Add Project'}</Title>
                </NavBox>
            </NavContainer>

            <ProjectForm />
        </>
    );
}
 
export default ProjectEditor;