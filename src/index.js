import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store'
import Error404 from './pages/Error404';
import ProjectEditor from './pages/ProjectEditor';


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/create-project" component={ProjectEditor} />
        <Route path="/edit-project/:id" component={ProjectEditor} />
        <Route component={Error404} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);