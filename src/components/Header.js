import React from 'react';
import Logo from '../logo.png';
import { Container } from '../styles/Globals';

const Header = () => {
    return (
        <Container>
            <img src={Logo} alt="Logo" />
        </Container>
    );
}
 
export default Header;