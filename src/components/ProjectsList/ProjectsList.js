import React from "react";
import { TableContainer } from "./styles";
import { Message } from "../../styles/Globals";
import { Table, Dropdown } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEdit,
  faTrashAlt,
  faEllipsisV,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom";
import { alertConfirm } from "../Alert";
import { deleteProject } from "../../redux/projectsReducer";

const ProjectsList = () => {
  const projects = useSelector((store) => store.projects.projects);
  const dispatch = useDispatch();
  const history = useHistory();

  const handleClick = (id) => {
    alertConfirm({
      title: "Are you sure?",
      text: "Project will be deleted",
      callback: () => {
        dispatch(deleteProject(id));
      },
    });
  };

  return (
    <TableContainer>
      {projects.length > 0 ? (
        projects.map((project) => {
          return (
            <Table bordered borderless hover>
              <thead>
                <tr>
                  <th>Project info</th>
                  <th>Project Manager</th>
                  <th>Assigned to</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr key={project.id}>
                  <td>
                    {project.title} <br/>
                    {project.date}
                  </td>
                  <td>
                    <FontAwesomeIcon icon={faUser} /> {project.manager}
                  </td>
                  <td>
                    <FontAwesomeIcon icon={faUser} /> {project.assigned}
                  </td>
                  <td>{project.status}</td>
                  <td>
                    <Dropdown>
                      <Dropdown.Toggle>
                        <FontAwesomeIcon icon={faEllipsisV} />
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item
                          onClick={() => {
                            history.push(`/edit-project/${project.id}`);
                          }}
                        >
                          {" "}
                          <FontAwesomeIcon icon={faEdit} /> Edit{" "}
                        </Dropdown.Item>
                        <Dropdown.Item onClick={() => handleClick(project.id)}>
                          {" "}
                          <FontAwesomeIcon icon={faTrashAlt} /> Delete{" "}
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </td>
                </tr>
              </tbody>
            </Table>
          );
        })
      ) : (
        <Message>Add a New Project</Message>
      )}
    </TableContainer>
  );
};

export default ProjectsList;
