import styled from "styled-components";

const TableContainer = styled.div`
  margin: 25px auto;
  background-color: #fff;

  @media (min-width: 768px) {
    width: 85%;
  }

  table {
    thead {
      display: none;

      @media (min-width: 768px) {
        display: table-header-group;
      }
    }

    tbody {
      td {
        padding: 5px 5px 5px 20px;

        &:nth-child(2n) {
          display: none;
        }

        @media (min-width: 768px) {
          padding: 12px;
          display: table-cell;

          &:nth-child(2n) {
            display: block;
          }
        }
      }
    }
  }
`;

export { TableContainer };
