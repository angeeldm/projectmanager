import Swal from "sweetalert2";

const alertSuccess = ({title, text}) => {
    Swal.fire({
        title: title,
        text: text,
        icon: 'success',
        timer: 1400,
    })
}

const alertError = ({title, text}) => {
    Swal.fire({
        title: title,
        text: text,
        icon: 'error',
        timer: 1400,
    })
}

const alertConfirm = ({title, text, callback = () => {}}) => {
    Swal.fire({
        title: title,
        text: text,
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: "Yes",
    }).then(res => {
        if(res.isConfirmed) {
            callback()
        }
    })
}

export {alertSuccess, alertError, alertConfirm}