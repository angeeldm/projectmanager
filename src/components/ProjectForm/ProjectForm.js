import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import { TextField, Button, MenuItem } from "@material-ui/core";
import { FormContainer, Form, FormField } from "./styles";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { addProject } from "../../redux/projectsReducer";
import { alertError, alertSuccess } from "../Alert";

const ProjectForm = () => {
  const project = useSelector((store) => store.projects.projects);
  const history = useHistory();
  const dispatch = useDispatch();
  const { id } = useParams();
  const [values, setValues] = useState({
    title: "",
    description: "",
    manager: "",
    assigned: "",
    status: "Enabled",
  });

  useEffect(() => {
    if (id) {
      setValues(project[0]);
    } else return;
  }, []);

  const persons = [
    { name: "Walt Cosani" },
    { name: "Ingnacio Truffa" },
    { name: "Natalia Azul" },
  ];
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: values.title,
      description: values.description,
      manager: values.manager,
      assigned: values.assigned,
      status: values.status,
    },
    onSubmit: (values) => {
      if (
        values.title === "" ||
        values.description === "" ||
        values.manager === "" ||
        values.assigned === ""
      ) {
        alertError({ title: "Fields can not be empty", text: "Try again!" });
      } else {
        dispatch(addProject(values));
        alertSuccess({ title: "Project Created" });
        setTimeout(() => {
          history.push("/");
        }, 1600);
      }
    },
  });

  return (
    <>
      <FormContainer>
        <Form onSubmit={formik.handleSubmit}>
          <FormField>
            <label>Project Name</label>
            <TextField
              variant="outlined"
              size="small"
              type="text"
              name="title"
              value={formik.values.title || ""}
              onChange={formik.handleChange}
            />
          </FormField>
          <FormField>
            <label>Description</label>
            <TextField
              variant="outlined"
              size="small"
              type="text"
              name="description"
              value={formik.values.description || ""}
              onChange={formik.handleChange}
            />
          </FormField>
          <FormField>
            <label>Project Manager</label>
            <TextField
              variant="outlined"
              size="small"
              select
              name="manager"
              value={formik.values.manager || ""}
              onChange={formik.handleChange}
            >
              {persons.map((option) => (
                <MenuItem key={option.name} value={option.name}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </FormField>
          <FormField>
            <label>Assigned to</label>
            <TextField
              variant="outlined"
              size="small"
              select
              name="assigned"
              value={formik.values.assigned || ""}
              onChange={formik.handleChange}
            >
              {persons.map((option) => (
                <MenuItem key={option.name} value={option.name}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </FormField>
          <FormField>
            <label>Status</label>
            <TextField
              variant="outlined"
              size="small"
              select
              name="status"
              value={formik.values.status || "Enabled"}
              onChange={formik.handleChange}
            >
              <MenuItem key="enable" value="Enabled">
                Enabled
              </MenuItem>
              <MenuItem key="disabled" value="Disabled">
                Disabled
              </MenuItem>
            </TextField>
          </FormField>

          <Button type="submit" variant="contained" color="secondary">
            {id ? 'Save Changes' : 'Create Project'}
          </Button>
        </Form>
      </FormContainer>
    </>
  );
};

export default ProjectForm;
