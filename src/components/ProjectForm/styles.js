import styled from "styled-components";

const FormContainer = styled.div`
  margin: 25px auto;

  @media (min-width: 768px) {
    width: 65%;
  }
`;

const Form = styled.form`
  background-color: #fff;
  padding: 25px;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.15);

  label {
    font-weight: bold;
    margin: 0;
  }

  button {
    background-color: #f5222d;
    color: #fff;
    margin-top: 15px;
  }
`;

const FormField = styled.div`
  display: flex;
  flex-direction: column;
  gap: 5px;
  margin: 10px 0;
`;

export { FormContainer, Form, FormField };
