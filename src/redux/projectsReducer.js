import { v4 as uuidv4 } from 'uuid';

const initialState = {
    projects: [{
        id: 1,
        title: 'Challenge Frontend',
        description: 'this is a description of the challenge',
        manager: 'Esto Es',
        assigned: 'Angel Mejia',
        status: 'Enabled',
        date: '04/06/2021'
    }]
}

const ADD_PROJECT = 'ADD_PROJECT';
const EDIT_PROJECT = 'EDIT_PROJECT';
const DELETE_PROJECT = 'DELETE_PROJECT';

const projectsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PROJECT:
            return {...state, projects: [action.payload]}

        case EDIT_PROJECT:
            const projectsFiltered = state.projects.filter(
                (project) => project.id !== action.payload.id
            );
            const projectEdited = { projects: action.payload };
            return { ...state, projects: projectEdited.concat(projectsFiltered) };

        case DELETE_PROJECT:
            const projectFiltered = state.projects.filter(
                (project) => project.id !== action.payload.id
            );
            return { ...state, projects: projectFiltered };
    
        default:
            return state
    }
}

const addProject = (values) => (dispatch) => {
    const id = uuidv4();
    dispatch({
        type: ADD_PROJECT,
        payload:{
            id: id,
            title: values.title,
            description: values.description,
            manager: values.manager,
            assigned: values.assigned,
            status: values.status,
        }
    })
}

const editProject = (values) => (dispatch) => {
    dispatch({
        type: EDIT_PROJECT,
        payload:{
            id: values.id,
            title: values.title,
            description: values.description,
            manager: values.manager,
            assigned: values.assigned,
            status: values.status,
        }
    })
}

const deleteProject = (id) => (dispatch) => {
    dispatch({
        type: DELETE_PROJECT,
        payload: {id: id}
    })
}

export {projectsReducer, addProject, editProject, deleteProject}