import { configureStore } from '@reduxjs/toolkit';
import { projectsReducer } from './projectsReducer';

export default configureStore({
    reducer:{
        projects: projectsReducer
    }
})